package com.implesoft.emstracking.activity;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.implesoft.emstracking.R;
import com.implesoft.emstracking.controller.WayBuildingController;
import com.implesoft.emstracking.fragment.MainFragment;
import com.implesoft.emstracking.model.Marker;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Log.d("MyLog", "result = " + new WayBuildingController().getLengthToMarker(new Marker(3, 3), new Marker(1, 5)));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment())
                    .commit();
        }

    }


}

