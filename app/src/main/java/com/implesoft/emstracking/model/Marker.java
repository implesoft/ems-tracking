package com.implesoft.emstracking.model;

/**
 * Created by Yana on 14.05.2015.
 */
public class Marker {

    private double latitude, longitude;

    public Marker(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
