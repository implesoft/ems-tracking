package com.implesoft.emstracking.fragment;


import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.implesoft.emstracking.R;
import com.implesoft.emstracking.controller.GPSController;
import com.implesoft.emstracking.controller.MarkerController;
import com.implesoft.emstracking.route.MyItemizedOverlay;

import org.osmdroid.api.IMapController;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

/**
 * Created by vika on 13.05.15.
 */
public class MainFragment extends Fragment {
    MapView mapView;
    IMapController controller;
    ImageView btnRefresh;
    GPSController gpsController;
    LocationManager locationManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        init(view);

        return view;
    }

    private void init(View view) {
        mapView = (MapView) view.findViewById(R.id.map);
        btnRefresh = (ImageView) view.findViewById(R.id.btnRefreshLocation);
        locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapView.setClickable(true);
        controller = mapView.getController();
        controller.setZoom(2);
        gpsController = new GPSController(getActivity(), mapView, locationManager);

        gpsController.showLocationMarker(gpsController.getLocationFromGPS());
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mapView.getOverlays().remove(MarkerController.anotherItemizedIconOverlay);
                mapView.refreshDrawableState();

                Location geoPoint = gpsController.getLocationFromGPS();
                if (gpsController.showLocationMarker(geoPoint)) {

                    controller.setZoom(30);
                    controller.animateTo(new GeoPoint(geoPoint));
                    mapView.invalidate();

                }
            }
        });
    }

}