package com.implesoft.emstracking.controller;

import android.app.Activity;
import android.content.Context;
import android.location.Location;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

/**
 * Created by Domain on 15.05.2015.
 */
public class GeoPointController {

    static GeoPoint geoPointMarker;
    public static int calculateDistance(Location location, GeoPoint geoPoint2) {
        return new GeoPoint(location.getLatitude(), location.getLongitude()).distanceTo(geoPoint2);
    }


    public static void setGeoPoint(GeoPoint geoPoint, MapView mapView, Context activity){
        geoPointMarker = geoPoint;
        ArrayList<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
        overlayItems.add(new OverlayItem(
                "Current position", "0, 0", geoPoint));

        ItemizedIconOverlay<OverlayItem> anotherItemizedIconOverlay = new ItemizedIconOverlay<OverlayItem>(activity, overlayItems, null);
        mapView.getOverlays().add(anotherItemizedIconOverlay);
    }

    public static GeoPoint getGeoPoint(){

        if(geoPointMarker != null){
            return geoPointMarker;
        }else{
            return null;
        }
    }
}

