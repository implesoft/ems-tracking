package com.implesoft.emstracking.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by HANZ on 24.05.2015.
 */
public class InternetController {

    public static boolean isMobile3G(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return mobile != null && mobile.isAvailable()
                && mobile.getDetailedState() == NetworkInfo.DetailedState.CONNECTED;

    }

    public static boolean isWifi(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return wifi != null && (wifi.isAvailable() && wifi
                .getDetailedState() == NetworkInfo.DetailedState.CONNECTED);

    }

    public static void toastInternetError(Context context) {
        Toast.makeText(context, "Please, check your interner connections",
                Toast.LENGTH_LONG).show();
    }

}
