package com.implesoft.emstracking.route;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;

/**
 * Created by HANZ on 23.05.2015.
 */
public class UpdateRoadTask extends AsyncTask<Object, Void, Road> {

    Context activity;
    MapView mapView;

    public UpdateRoadTask(Context activity, MapView mapView) {
        this.activity = activity;
        this.mapView = mapView;
    }

    Road road;

    protected Road doInBackground(Object... params) {
        @SuppressWarnings("unchecked")
        ArrayList<GeoPoint> waypoints = (ArrayList<GeoPoint>) params[0];
        RoadManager roadManager = new OSRMRoadManager();


        return roadManager.getRoad(waypoints);
    }

    @Override
    protected void onPostExecute(Road result) {
        road = result;
        // showing distance and duration of the road
        //Toast.makeText(activity, "distance=" + road.mLength, Toast.LENGTH_LONG).show();

        if (road.mStatus != Road.STATUS_OK) {
            Toast.makeText(activity, "Check your Internet connection", Toast.LENGTH_LONG).show();
        } else {
            Polyline roadOverlay = RoadManager.buildRoadOverlay(road, activity);
            mapView.getOverlays().add(roadOverlay);
        }
        mapView.invalidate();
        //updateUIWithRoad(result);
    }
}
